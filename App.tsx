/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {SafeAreaView, Text, Button, View} from 'react-native';

import {store} from './src/redux/store/store';
import {Provider} from 'react-redux';
import {useAppSelector, useAppDispatch} from './src/redux/store/hooks';

import {add} from './src/redux/slices/productsSlice';

import {Table, Row} from 'react-native-table-component';

const Simple: React.FC = () => {
  const dispatch = useAppDispatch();
  const {title} = useAppSelector(state => state.task);
  const products = useAppSelector(state => state.products.products);

  const changeState = () => {
    dispatch({type: 'complete'});
  };

  return (
    <SafeAreaView>
      <Text>Hello World!</Text>
      <Text>{title}</Text>
      <Button onPress={changeState} title="Press me" />
      <Button
        onPress={() => {
          dispatch(add());
        }}
        title="Add products"
      />
      <View>
        <Table>
          <Row data={['name', 'amount']} />
          {products.map(product => {
            return <Text key={product.key}>{product.name}</Text>;
          })}
        </Table>
      </View>
    </SafeAreaView>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <Simple />
    </Provider>
  );
};

export default App;
