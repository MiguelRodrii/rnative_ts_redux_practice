import {configureStore} from '@reduxjs/toolkit';
import {AnyAction} from 'redux';
import productsReducer from '../slices/productsSlice';

interface Task {
  title: string;
  is_completed: boolean;
}
const initialState: Task = {
  title: 'Configure redux with react',
  is_completed: false,
};

const taskReducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case 'complete':
      return {...state, is_completed: true, title: 'Cambiado'};
    default:
      return state;
  }
};

export const store = configureStore({
  reducer: {
    task: taskReducer,
    products: productsReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
