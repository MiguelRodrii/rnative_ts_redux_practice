import {createSlice} from '@reduxjs/toolkit';

interface Product {
  key: number;
  name: string;
  amount: number;
}

interface Products {
  products: Product[];
}

const initialState: Products = {
  products: [],
};

export const productsSlice = createSlice({
  name: 'products',
  initialState: initialState,
  reducers: {
    add: state => {
      var products = state.products.slice();
      products.push({
        key: 1,
        name: 'n1',
        amount: 1,
      });
      return {
        ...state,
        products: products,
      };
    },
  },
});

export const {add} = productsSlice.actions;

export default productsSlice.reducer;
